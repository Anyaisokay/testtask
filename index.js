const products = [
  {
    id: 0,
    name: 'Sneakers',
    price: 180,
    image: './images/692e989c45494e8c8423ad1000c3d058_9366.webp',
  },
  {
    id: 1,
    name: 'Basketball',
    price: 60,
    image: './images/49c882a9bb6b4d96930bad9601601459_9366.webp',
  },
  {
    id: 2,
    name: 'Cap',
    price: 45.7,
    image: './images/596661986bd746788f8fada1011e20a6_9366.webp',
  },
  {
    id: 3,
    name: 'Bottle',
    price: 26.4,
    image: './images/dff453a4d41045f08284ad1a00aa5b2b_9366.webp',
  },
  {
    id: 4,
    name: 'Gloves',
    price: 56,
    image: './images/a4721c64063f4aee9481ac9900eb2d43_9366.webp',
  },
  {
    id: 5,
    name: 'Headband',
    price: 37.5,
    image: './images/Povyazka_na_golovu_dlya_tennisa_chernyj_HD7327_01_standard.jpg',
  },
  {
    id: 6,
    name: 'Swimming goggles',
    price: 57.2,
    image: './images/04bc275cd6e3494c88f3a880014c0673_9366.webp',
  },
  {
    id: 7,
    name: 'Backpack',
    price: 73,
    image: './images/Ryukzak_Adicolor_Classic_Small_fioletovyj_HD7176_01_standard.jpg',
  },
];

const openCart = document.querySelector('.openCart');
const cart = document.querySelector('.cart');
const cartList = document.querySelector('.cart__list');
const $products = document.querySelector('.main__cards');
const cartTotal = document.querySelector('.cartTotal');
const resultPrice = document.querySelector('.cart__totalPrice');
const buyButton = document.querySelector('.buyButton');
let totalPrice = 0;

const handleCloseCartClick = () => {
  cart.style.display = 'none';
};

function getProductsLocalStorage() {
  return JSON.parse(localStorage.getItem('products')) || [];
}

const cartPrice = () => {
  const newCartArray = getProductsLocalStorage();
  if (newCartArray.length === 0) {
    totalPrice = 0;
  } else {
    totalPrice = newCartArray.reduce((acc, curr) => acc + curr.price * curr.quantity, 0).toFixed(2);
  }

  resultPrice.innerHTML = `<b>Total</b>: ${totalPrice}$`;
  return totalPrice;
};

const buyMessage = () => {
  const total = cartPrice();
  alert(`Итого к оплате ${total}$`);
};

// buyButton.addEventListener('click', buyMessage());

if (localStorage.getItem('total')) totalPrice = localStorage.getItem('total');
else totalPrice = 0;
resultPrice.innerHTML = totalPrice;

const renderProducts = () => {
  const items = products.map((product) => {
    const {
      id, price, name, image,
    } = product;
    const li = document.createElement('li');
    li.classList.add('card');
    li.innerHTML = `
            <img src="${image}" alt="">
            <div class="card__caption">
                <div class="card__description">
                    <h3>${name}</h3>
                    <p class="card__price">${price}$</p>
                </div>
                <button class="card__addButton" onclick="addToCart(${id})"><img src="./images/bag-shopping-solid.svg" alt="" class="card__bag" ></button>
            </div>
    `;
    return li;
  });

  $products.append(...items);
};

renderProducts();

const sort = () => {
  const cartArray = getProductsLocalStorage();
  const resultArray = cartArray.reduce((acc, obj) => {
    const index = acc.findIndex((el) => el.id === obj.id);
    if (index === -1) {
      return acc.concat({ ...obj });
    }
    acc[index].price += obj.price;
    return acc;
  }, []);
  return resultArray;
};

const renderCart = () => {
  cart.style.display = 'flex';
  const resultArray = sort();
  if (resultArray.length === 0) {
    cartList.innerHTML = `
          <li class="emptyCartMessage">Корзина пуста</li>
      `;
  } else {
    cartTotal.style.display = 'block';
    buyButton.style.display = 'block';
    cartList.innerHTML = '';
    resultArray.forEach((item) => {
      const li = document.createElement('li');
      li.classList.add('cart__item');
      li.id = `${item.id}`;
      li.innerHTML = `<div class="cart__list-item">
              <img class="cart__img" src="${item.image}" alt="">
              <div class="cart__list-item-description">
                   <h3>${item.name}</h3>
                   <p class="cart__list-item-price">${(item.price * item.quantity).toFixed(2)}$</p>
                   <p>${item.quantity}</p>
                   <button class="cart__addButton" onclick="addToCart(${
  item.id
})"><img src="./images/plus-solid.svg"></button>
                   <button class="cart__addButton" onclick="deleteProduct(${
  item.id
}, ${1})"><img src="./images/minus-solid.svg"></button>
               </div>
               <button class="cart__trashButton" id="${item.id}" onclick="deleteProduct(${
  item.id
}, ${item.quantity})"><img src="./images/trash-solid.svg" alt=""></button>
           </div>`;

      cartList.appendChild(li);
    });
  }
  cartPrice();
};

const addToCart = (id) => {
  const product = products.find((it) => it.id === parseInt(id));

  const cartArray = getProductsLocalStorage();

  const newProduct = { ...product, quantity: 1 };

  const existingProduct = cartArray.findIndex((item) => item.id === id);
  if (existingProduct !== -1) {
    const resultArray = cartArray.map((item, index) => {
      if (index === existingProduct) {
        return { ...item, quantity: item.quantity + 1 };
      }
      return item;
    });
    localStorage.setItem('products', JSON.stringify(resultArray));
  } else {
    localStorage.setItem('products', JSON.stringify([...cartArray, newProduct]));
  }

  localStorage.setItem('total', JSON.stringify(totalPrice));

  renderCart();
};

const deleteProduct = (id, count) => {
  const cartArray = getProductsLocalStorage();
  const cartItem = cartArray.find((item) => item.id === id);

  const updateQuantity = (item) => {
    if (item.id === id) {
      return { ...item, quantity: item.quantity - count };
    }
    return item;
  };

  if (!cartItem) {
    return;
  }
  if (cartItem.quantity > count) {
    const result = cartArray.map(updateQuantity);

    localStorage.setItem('products', JSON.stringify(result));
  } else {
    localStorage.setItem('products', JSON.stringify(cartArray.filter((item) => item.id !== id)));
    cartTotal.style.display = 'none';
    buyButton.style.display = 'none';
  }

  renderCart();
};
